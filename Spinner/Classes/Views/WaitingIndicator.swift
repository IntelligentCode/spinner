//
//  LoadingSpinner.swift
//  LoadIndicator
//
//  Created by Артем on 23/11/2016.
//  Copyright © 2016 Artem Salimyanov. All rights reserved.
//

import UIKit

public class WaitingIndicator: UIView {
    
    // MARK: Public Variables
    
    public var text: String? {
        get { return _text ?? nil }
        set {
            _text = newValue
        }
    }
    
    public var speedAnimation: Float {
        get { return _speedAnimation }
        set {
            _speedAnimation = newValue
        }
    }
    
    // MARK: Private Variables
    
    private var blurEffectView: UIVisualEffectView?
    private var stackView: UIStackView!
    private var spinnerView: UIView!
    private var spinnerLayer: CAShapeLayer!
    private var textLabel: UILabel?
    
    private var _text: String? {
        didSet {
            self.textLabel?.text = _text
        }
    }
    private var _speedAnimation: Float = 1.0 {
        didSet {
            spinnerLayer.timeOffset = spinnerLayer.convertTime(CACurrentMediaTime(), from: nil)
            spinnerLayer.beginTime = CACurrentMediaTime();
            spinnerLayer.speed = _speedAnimation
        }
    }
    private var scale: CGFloat = 0.17
    private var squareSpinSide: CGFloat {
        return min(self.bounds.width, self.bounds.height) * scale
    }
    
    // animation
    
    private var strokeEndAnimation: CAAnimation = {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1.0
        animation.duration = 2.0
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        let group = CAAnimationGroup()
        group.duration = 2.5
        group.repeatCount = MAXFLOAT
        group.animations = [animation]
        
        return group
    }()
    
    private let strokeStartAnimation: CAAnimation = {
        let animation = CABasicAnimation(keyPath: "strokeStart")
        animation.beginTime = 0.5
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = 2.0
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        let group = CAAnimationGroup()
        group.duration = 2.5
        group.repeatCount = MAXFLOAT
        group.animations = [animation]
        
        return group
    }()
    
    private var animating: Bool = true {
        didSet {
            updateAnimation()
        }
    }
    
    // MARK: - Actions
    
    fileprivate func startFadeInAnimation() {
        self.alpha = 0.0
        let animationDuration = UIApplication.shared.statusBarOrientationAnimationDuration
        UIView.animate(withDuration: animationDuration, animations: {
            self.alpha = 1.0
        })
    }
    
    fileprivate func startFadeOutAnimation() {
        let animationDuration = UIApplication.shared.statusBarOrientationAnimationDuration
        UIView.animate(withDuration: animationDuration, animations: {
            self.alpha = 0.0
        }) { (finished) in
            self.stop()
        }
    }
    
    // MARK: Init
    
    public convenience init(withColor color: CGColor) {
        self.init()
        self.spinnerLayer.strokeColor = color
    }
    
    public convenience init(withColor color: CGColor, andBackgroundColor backgroundColor: UIColor, withOpacity opacity: CGFloat) {
        self.init()
        blurEffectView?.removeFromSuperview()
        self.spinnerLayer.strokeColor = color
        self.backgroundColor = backgroundColor.withAlphaComponent(opacity)
    }
    
    public convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    fileprivate override init(frame: CGRect) {
        super.init(frame: frame)
        performInitialization()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        performInitialization()
    }
    
    private func performInitialization() {
        self.translatesAutoresizingMaskIntoConstraints = false
        configureBackground()
        configureStackView()
    }
    
    // MARK: Layout
    
    public override func didMoveToWindow() {
        super.didMoveToWindow()
        
        configureSelfLayout()
        configureLayoutBlurEffectView()
        configureLayoutStackView()
        drawSpinner()
        configureLayoutTextLabel()
        updateAnimation()
    }
    
    // MARK: Configure
    
    private func configureBackground() {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        self.blurEffectView = blurEffectView
        self.addSubview(blurEffectView)
    }
    
    private func configureStackView() {
        let wrapperView = UIStackView()
        wrapperView.translatesAutoresizingMaskIntoConstraints = false
        wrapperView.axis = .vertical
        wrapperView.distribution = .fillEqually
        wrapperView.alignment = .fill
        
        self.stackView = wrapperView
        self.addSubview(stackView)
        
        configureSpinnerView()
        configureTextLabel()
    }
    
    private func configureSpinnerView() {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        
        self.spinnerView = view
        self.stackView.addArrangedSubview(spinnerView)
        
        configureSpinnerLayer()
    }
    
    private func configureSpinnerLayer()  {
        let layer = CAShapeLayer()
        layer.lineWidth = 4.0
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = UIColor(red:0.61, green:0.11, blue:0.61, alpha:1.0).cgColor
        
        self.spinnerLayer = layer
        self.spinnerView.layer.addSublayer(spinnerLayer)
    }
    
    private func configureTextLabel() {
        let label = UILabel()
        label.text = text
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue", size: 16.0)
        label.textColor = UIColor.gray
        label.numberOfLines = 0
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.adjustsFontSizeToFitWidth = true
        
        self.textLabel = label
        
        self.addSubview(textLabel!)
    }
    
    // MARK: Layout Config
    
    private func configureSelfLayout() {
        guard let superview = self.superview else { return }
        
        let leading = NSLayoutConstraint(item: self,
                                         attribute: .leading,
                                         relatedBy: .equal,
                                         toItem: superview,
                                         attribute: .leading,
                                         multiplier: 1.0,
                                         constant: 0.0)
        
        let trailing = NSLayoutConstraint(item: self,
                                          attribute: .trailing,
                                          relatedBy: .equal,
                                          toItem: superview,
                                          attribute: .trailing,
                                          multiplier: 1.0,
                                          constant: 0.0)
        
        let top = NSLayoutConstraint(item: self,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: superview,
                                     attribute: .top,
                                     multiplier: 1.0,
                                     constant: 0.0)
        
        let bottom = NSLayoutConstraint(item: self,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: superview,
                                        attribute: .bottom,
                                        multiplier: 1.0,
                                        constant: 0.0)
        
        superview.addConstraints([leading, trailing, top, bottom])
        self.layoutIfNeeded()
    }
    
    private func configureLayoutBlurEffectView() {
        guard blurEffectView?.superview != nil else { return }
        
        let leading = NSLayoutConstraint(item: blurEffectView!,
                                         attribute: .leading,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .leading,
                                         multiplier: 1.0,
                                         constant: 0.0)
        
        let trailing = NSLayoutConstraint(item: blurEffectView!,
                                          attribute: .trailing,
                                          relatedBy: .equal,
                                          toItem: self,
                                          attribute: .trailing,
                                          multiplier: 1.0,
                                          constant: 0.0)
        
        let top = NSLayoutConstraint(item: blurEffectView!,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: self,
                                     attribute: .top,
                                     multiplier: 1.0,
                                     constant: 0.0)
        
        let bottom = NSLayoutConstraint(item: blurEffectView!,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: self,
                                        attribute: .bottom,
                                        multiplier: 1.0,
                                        constant: 0.0)
        
        self.addConstraints([leading, trailing, top, bottom])
    }
    
    private func configureLayoutStackView() {
        
        NSLayoutConstraint(item: stackView,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .centerX,
                           multiplier: 1.0,
                           constant: 0.0).isActive = true
        
        NSLayoutConstraint(item: stackView,
                           attribute: .centerY,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .centerY,
                           multiplier: 1.0,
                           constant: 0.0).isActive = true
        
        NSLayoutConstraint(item: stackView,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 1.0,
                           constant: squareSpinSide).isActive = true
        
        NSLayoutConstraint(item: stackView,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 1.0,
                           constant: squareSpinSide).isActive = true
        
        stackView.layoutIfNeeded()
    }
    
    private func configureLayoutTextLabel() {
        
        let top = NSLayoutConstraint(item: textLabel!,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: stackView,
                                     attribute: .bottom,
                                     multiplier: 1.0,
                                     constant: 8.0)
        
        let centerX = NSLayoutConstraint(item: textLabel!,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0.0)
        
        let trailing = NSLayoutConstraint(item: textLabel!,
                                          attribute: .trailing,
                                          relatedBy: .equal,
                                          toItem: self,
                                          attribute: .trailing,
                                          multiplier: 1.0,
                                          constant: -15.0)
        
        let leading = NSLayoutConstraint(item: textLabel!,
                                         attribute: .leading,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .leading,
                                         multiplier: 1.0,
                                         constant: 15.0)
        
        self.addConstraints([centerX, top, trailing, leading])
    }
    
    private func drawSpinner() {
        let radius = (squareSpinSide - spinnerLayer.lineWidth) / 2
        let startAngle = CGFloat(-Double.pi / 2)
        let endAngle = startAngle + CGFloat(Double.pi * 2)
        let path = UIBezierPath(arcCenter: spinnerView.center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        spinnerLayer.path = path.cgPath
    }
    
    private func updateAnimation() {
        if animating {
            spinnerLayer.add(strokeEndAnimation, forKey: "strokeEnd")
            spinnerLayer.add(strokeStartAnimation, forKey: "strokeStart")
        }
        else {
            spinnerLayer.removeAnimation(forKey: "strokeEnd")
            spinnerLayer.removeAnimation(forKey: "strokeStart")
        }
    }
    
    // MARK: Public Actions
    
    public func start() {
        let window = UIApplication.shared.keyWindow
        window?.addSubview(self)
    }
    
    public func stop() {
        removeText()
        self.removeFromSuperview()
    }
    
    public func removeText() {
        self.text = nil
    }
    
    public func fadeIn() {
        start()
        startFadeInAnimation()
    }
    
    public func fadeOut() {
        startFadeOutAnimation()
    }
}
